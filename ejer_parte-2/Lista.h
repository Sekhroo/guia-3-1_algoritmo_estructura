#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;

/* Crear la estructura de nodos para la lista */
typedef struct _Nodo {
    string dato;
    struct _Nodo *next;
} Nodo;

class Lista {

    private:
        Nodo *first_data = NULL;
        Nodo *last_data = NULL;

    public:
        Lista();

        void add_nodo();

        void mostrar_lista();

};
#endif