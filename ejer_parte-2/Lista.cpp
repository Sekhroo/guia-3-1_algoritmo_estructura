#include <iostream>
#include <algorithm>
using namespace std;

#include "Lista.h"

Lista::Lista(){}

/* Función para añadir un dato a la lista */
void Lista::add_nodo(){
    string a;
    string b;

    Nodo *directriz = new Nodo;

    cout << "Ingrese una nueva variable a la lista: ";    
    cin >> directriz->dato;

    directriz->next = NULL;

    /* Función para verificar si es el primero */
    if (this->first_data == NULL) { 
        this->first_data = directriz;
        this->last_data = this->first_data;

    } else {
     
        Nodo *indice, *pre_indice;

        indice = first_data;

        a = directriz->dato;
        b = indice->dato;

        transform(a.begin(),a.end(),a.begin(),::tolower);
        transform(b.begin(),b.end(),b.begin(),::tolower);

        while((indice != NULL) && (b < a)){
            pre_indice = indice;
            indice = indice->next;
        }

        if(indice == first_data){
            first_data = directriz;
            first_data->next = indice;
        
        }else{
            pre_indice->next = directriz;
            directriz->next = indice;
        }
    }
    cout << "Se ha guardado el " << directriz->dato << " en la lista." << endl << endl; 
}

/* Función para mostrar todos los nodos de la lista */
void Lista::mostrar_lista(){
    cout << "Variables almacenadas en la lista: " << endl;

    Nodo *indice = new Nodo();

    indice = first_data;

    if(first_data != NULL){
        while(indice != NULL){
            cout << "[" << indice->dato << "]";
            if (indice->next == NULL){
                cout << ".";
            }else {
                cout << " - ";
            }
            indice = indice->next;
        }
        cout << endl << endl;

    }else{
        cout << "Lista sin variables. " << endl << endl;   
    }
}