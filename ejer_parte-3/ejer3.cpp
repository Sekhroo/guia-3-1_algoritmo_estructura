#include<iostream>
using namespace std;

#include "Postres.h"

/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt) {
    cout << "========== Menu ==========" << endl;
    cout << "Agregar postre  ______ [1]" << endl;
    cout << "Eliminar postre  _____ [2]" << endl;
    cout << "Mostrar postres  _____ [3]" << endl;
    cout << "Seleccionar postre  __ [4]" << endl;
    cout << "Salir del programa ___ [0]" << endl;
    cout << "==========================" << endl;
    cout << "Opción: ";

    cin >> opt;
    
    clear();

    return opt;
}

/* Función para la seleccion del postre */
string menu_selec_postre (string selec_postre, Postres *postres) {
    cout << "======= Seleccionar ==========" << endl;
    cout << "Nombre del postre: ";   
    cin >> selec_postre;
    selec_postre = postres->conversion_lower(selec_postre);

    clear();

    return selec_postre;
}

int main(){
    string option = "\0";
    string selec_postre = "\0";

    Postres *postres = new Postres();

    while (option != "0") {

        option = menu_principal(option);

        if (option == "1") {
            postres->add_nodo_postre();
        }

        if (option == "2") {
            selec_postre = menu_selec_postre(selec_postre, postres);
            postres->eliminar_nodo_postre(selec_postre);
        }

        if (option == "3") {
            postres->mostrar_lista_postre();
        }

        if (option == "4") {
            selec_postre = menu_selec_postre(selec_postre, postres);
            postres->redireccionar_funcion(selec_postre);
        }
    }
    return 0;
}