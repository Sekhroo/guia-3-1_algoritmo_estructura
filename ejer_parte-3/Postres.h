#ifndef POSTRES_H
#define POSTRES_H

#include <iostream>
using namespace std;

/* Crear la estructura de nodos para la lista de ingredientes */
typedef struct _Nodo_Ingrediente {
    string dato;
    struct _Nodo_Ingrediente *next;
} Nodo_Ingrediente;

/* Crear la estructura de nodos para la lista de postres */
typedef struct _Nodo_Postres {
    string dato;
    struct _Nodo_Postres *next;
    Nodo_Ingrediente *first_data_ingrediente;
    Nodo_Ingrediente *last_data_ingrediente;
} Nodo_Postres;

class Postres {

    private:
        Nodo_Postres *first_data_postres = NULL;
        Nodo_Postres *last_data_postres = NULL;

    public:
        Postres();

        /* Funciones para manipular los postres */
        void add_nodo_postre();
        void eliminar_nodo_postre(string selec_postre);
        void mostrar_lista_postre();

        /* Funciones para manipular los ingredientes */
        void add_nodo_ingrediente(string selec_postre);
        void eliminar_nodo_ingrediente(string selec_postre, string selec_ingrediente);
        void mostrar_lista_ingrediente(string selec_postre);

        /* Funciones complementarias */
        string conversion_lower(string cadena_string);
        void clear();

        /* Funcion central y menus*/
        void redireccionar_funcion(string selec_postre);
        string menu_ingrediente(string opt_selec, string selec_postre);
        string menu_selec_ingrediente(string selec_ingrediente);

};
#endif