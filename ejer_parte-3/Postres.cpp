#include <iostream>
#include <algorithm>
using namespace std;

#include "Postres.h"

Postres::Postres(){}

/* Función para limpiar la terminal */
void Postres::clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para convertir los strings en minusculas */
string Postres::conversion_lower(string cadena_string) {
    
    transform(cadena_string.begin(),cadena_string.end(),cadena_string.begin(),::tolower);

    return cadena_string;
}

/* Función para el menu secundario */
string Postres::menu_ingrediente (string opt_selec, string selec_postre) {
    cout << "=========== Postre ===========" << endl;
    cout << "Seleccionado: " << selec_postre << endl;
    cout << "============ Menu ============" << endl;
    cout << "Agregar ingrediente ______ [1]" << endl;
    cout << "Eliminar ingrediente _____ [2]" << endl;
    cout << "Mostrar ingredientes _____ [3]" << endl;
    cout << "Volver al menu principañ _ [0]" << endl;
    cout << "==============================" << endl;
    cout << "Opción: ";

    cin >> opt_selec;
    
    clear();

    return opt_selec;
}

/* Función para la seleccion del ingrediente */
string Postres::menu_selec_ingrediente (string selec_ingrediente) {
    cout << "======= Seleccionar ==========" << endl;
    cout << "Nombre del ingrediente: ";   
    cin >> selec_ingrediente;
    selec_ingrediente = conversion_lower(selec_ingrediente);

    return selec_ingrediente;
}



/* Función para añadir un dato a la lista de postres */
void Postres::add_nodo_postre(){

    Nodo_Postres *directriz = new Nodo_Postres;

    cout << "Ingrese un postre a la lista: ";    
    cin >> directriz->dato;

    directriz->next = NULL;

    /* Función para verificar si es el primero */
    if (this->first_data_postres == NULL) { 
        this->first_data_postres = directriz;
        this->last_data_postres = this->first_data_postres;

    } else {
     
        Nodo_Postres *indice, *pre_indice;

        indice = first_data_postres;

        directriz->dato = conversion_lower(directriz->dato);
        indice->dato = conversion_lower(indice->dato);

        while((indice != NULL) && (indice->dato < directriz->dato)){
            pre_indice = indice;
            indice = indice->next;
        }

        if(indice == first_data_postres){
            first_data_postres = directriz;
            first_data_postres->next = indice;
        
        }else{
            pre_indice->next = directriz;
            directriz->next = indice;
        }
    }
    cout << "Se ha guardado el postre '" << directriz->dato << "' en la lista." << endl << endl; 
}

/* Función para eliminar un dato a la lista de postres */
void Postres::eliminar_nodo_postre(string selec_postre){

    Nodo_Postres *indice, *pre_indice;

    indice = first_data_postres;

    if(first_data_postres != NULL){
        while(indice != NULL){
            if (indice->dato == selec_postre){
                if(indice == first_data_postres){
                    first_data_postres = indice->next;  
                }else{
                    pre_indice->next = indice->next;
                }
                cout << "Se ha eliminado el postre '" << indice->dato << "' de la lista." << endl << endl;
                break;
            }else{
                pre_indice = indice;
                indice = indice->next;
            }
        }
    }else{
        cout << "Lista sin postres. " << endl << endl; 
    }
}

/* Función para mostrar todos los nodos de la lista de postres */
void Postres::mostrar_lista_postre(){
    cout << "Postres almacenados en la lista: " << endl;

    Nodo_Postres *indice = new Nodo_Postres();

    indice = first_data_postres;

    if(first_data_postres != NULL){
        while(indice != NULL){
            cout << "[" << indice->dato << "]";
            if(indice->next != NULL){
                cout << " - ";
            }else{
                cout << ".";
            }
            indice = indice->next;
        }
        cout << endl << endl;

    }else{
        cout << "Lista sin postres. " << endl << endl;   
    }
}



/* Función para añadir un dato a la lista de ingredientes */
void Postres::add_nodo_ingrediente(string selec_postre){

    Nodo_Postres *indice_postres = new Nodo_Postres();

    indice_postres = first_data_postres;

    while(indice_postres != NULL){
        if (indice_postres->dato == selec_postre){

            Nodo_Ingrediente *directriz = new Nodo_Ingrediente;

            cout << "Ingrese un nuevo ingrediente a la lista: ";    
            cin >> directriz->dato;

            directriz->next = NULL;

            /* Función para verificar si es el primero */
            if (indice_postres->first_data_ingrediente == NULL) { 
                indice_postres->first_data_ingrediente = directriz;
                indice_postres->last_data_ingrediente = indice_postres->first_data_ingrediente;

            } else {
            
                Nodo_Ingrediente *indice, *pre_indice;

                indice = indice_postres->first_data_ingrediente;

                while((indice != NULL) && (indice->dato < directriz->dato)){
                    pre_indice = indice;
                    indice = indice->next;
                }

                if(indice == indice_postres->first_data_ingrediente){
                    indice_postres->first_data_ingrediente = directriz;
                    indice_postres->first_data_ingrediente->next = indice;
                
                }else{
                    pre_indice->next = directriz;
                    directriz->next = indice;
                }
            }
            cout << "Se ha guardado el ingrediente '" << directriz->dato << "' en la lista." << endl << endl;

            break;
        }else{
            indice_postres = indice_postres->next;
        }
    }   
}

/* Función para eliminar un dato a la lista de ingredientes */
void Postres::eliminar_nodo_ingrediente(string selec_postre, string selec_ingrediente){

    Nodo_Postres *indice_postres = new Nodo_Postres();

    indice_postres = first_data_postres;

    while(indice_postres != NULL){
        if (indice_postres->dato == selec_postre){
            
            Nodo_Ingrediente *indice, *pre_indice;

            indice = indice_postres->first_data_ingrediente;

            if(indice_postres->first_data_ingrediente != NULL){
                while(indice != NULL){
                    if (indice->dato == selec_ingrediente){
                        if(indice == indice_postres->first_data_ingrediente){
                            indice_postres->first_data_ingrediente = indice->next;   
                        }else{
                            pre_indice->next = indice->next;
                        }
                        cout << "Se ha eliminado el ingrediente '" << indice->dato << "' de la lista." << endl << endl;
                        break;
                    }else{
                        pre_indice = indice;
                        indice = indice->next;
                    }
                }
            }else{
                cout << "Lista sin ingredientes. " << endl << endl; 
            }
            break;
        }
    }
}

/* Función para mostrar todos los ingredientes del postre */
void Postres::mostrar_lista_ingrediente(string selec_postre){
    
    Nodo_Postres *indice_postres = new Nodo_Postres();

    indice_postres = first_data_postres;

    while(indice_postres != NULL){
        if (indice_postres->dato == selec_postre){
            cout << "Ingredientes almacenados en la lista: " << endl;

            Nodo_Ingrediente *indice = new Nodo_Ingrediente();

            indice = indice_postres->first_data_ingrediente;

            if(indice_postres->first_data_ingrediente != NULL){
                while(indice != NULL){
                    cout << "--> " << indice->dato << "." << endl;
                    indice = indice->next;
                }
                cout << endl;

            }else{
                cout << "Lista sin ingredientes. " << endl << endl;   
            }
            break;
        }else{
            indice_postres = indice_postres->next;
        }
    }   
}



/* Función manipular los ingredientes en la lista de postres */
void Postres::redireccionar_funcion(string selec_postre){
    string option_selec = "\0";
    string selec_ingrediente = "\0";

    while (option_selec != "0") {

        option_selec = menu_ingrediente (option_selec, selec_postre);

        if (option_selec == "1") {
            add_nodo_ingrediente(selec_postre);
        }

        if (option_selec == "2") {
            selec_ingrediente = menu_selec_ingrediente(selec_ingrediente);
            eliminar_nodo_ingrediente(selec_postre, selec_ingrediente);
        }

        if (option_selec == "3") {
            mostrar_lista_ingrediente(selec_postre);
        }
    }
}