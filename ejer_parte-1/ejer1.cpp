#include<iostream>
using namespace std;

#include "Lista.h"

/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt) {
    cout << "================== Menu ==================" << endl;
    cout << "Agregar nodo (Lista desordenada) _____ [1]" << endl;
    cout << "Mostrar conjunto (Lista desordenada) _ [2]" << endl << endl;

    cout << "Dividir lista desordenada y mostrar __ [0]" << endl;
    cout << "==========================================" << endl;
    cout << "Opción: ";

    cin >> opt;
    
    clear();

    return opt;
}

int main(){
    string option = "\0";
    string fin = "\0";

    Lista *lista_desordenada = new Lista();
    Lista *lista_ordenada_positiva = new Lista();
    Lista *lista_ordenada_negativa = new Lista();

    while (option != "0") {

        option = menu_principal(option);

        if (option == "1") {
            lista_desordenada->add_nodo_desordenado();

        }else if (option == "2") {
            lista_desordenada->mostrar_lista("desordenada");

        }else if (option == "0") {
            lista_ordenada_positiva->add_nodo_ordenado_positivo(lista_desordenada);
            lista_ordenada_negativa->add_nodo_ordenado_negativo(lista_desordenada);

            lista_desordenada->mostrar_lista("desordenada");
            lista_ordenada_positiva->mostrar_lista("ordenada positiva");
            lista_ordenada_negativa->mostrar_lista("ordenada negativa");

            cout << "Introduzca cualquier carácter para acabar: ";
            cin >> fin;
        }
    }
    return 0;
}