#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista(){}

/* Función para añadir un dato a la lista */
void Lista::add_nodo_desordenado(){

    Nodo *directriz = new Nodo;

    cout << "Ingrese una nueva variable a la lista: ";    
    cin >> directriz->dato;

    directriz->next = NULL;

    /* Función para verificar si es el primero */
    if (this->first_data == NULL) { 
        this->first_data = directriz;
        this->last_data = this->first_data;

    } else {     
        this->last_data->next = directriz;
        this->last_data = directriz;
    }
    cout << "Se ha guardado el " << directriz->dato << " en la lista." << endl << endl; 
}

/* Función para crear o añadir a la lista positiva */
void Lista::add_nodo_ordenado_positivo(Lista *nodos_base){
    
    Nodo *tmp = new Nodo();
    tmp = nodos_base->first_data;

    while(tmp!= NULL){

        Nodo *directriz = new Nodo;

        directriz->dato = tmp->dato;

        directriz->next = NULL;

        if (directriz->dato > 0){

            if (this->first_data == NULL) { 
                this->first_data = directriz;
                this->last_data = this->first_data;

            } else {
            
                Nodo *indice, *pre_indice;

                indice = first_data;

                while((indice != NULL) && (indice->dato < directriz->dato)){
                    pre_indice = indice;
                    indice = indice->next;
                }

                if(indice == first_data){
                    first_data = directriz;
                    first_data->next = indice;
                
                }else{
                    pre_indice->next = directriz;
                    directriz->next = indice;
                }
            }
        }
        tmp = tmp->next;
    }
}

/* Función para crear o añadir a la lista negativa */
void Lista::add_nodo_ordenado_negativo(Lista *nodos_base){

    Nodo *tmp = new Nodo();
    tmp = nodos_base->first_data;

    while(tmp!= NULL){

        Nodo *directriz = new Nodo;

        directriz->dato = tmp->dato;

        directriz->next = NULL;

        if (directriz->dato < 0){

            if (this->first_data == NULL) { 
                this->first_data = directriz;
                this->last_data = this->first_data;

            } else {
            
                Nodo *indice, *pre_indice;

                indice = first_data;

                while((indice != NULL) && (indice->dato < directriz->dato)){
                    pre_indice = indice;
                    indice = indice->next;
                }

                if(indice == first_data){
                    first_data = directriz;
                    first_data->next = indice;
                
                }else{
                    pre_indice->next = directriz;
                    directriz->next = indice;
                }
            }
        }
        tmp = tmp->next;
    }
}

/* Función para mostrar todos los nodos de la lista */
void Lista::mostrar_lista(string nombre_lista){
    cout << "Variables almacenadas en la lista "<< nombre_lista <<": " << endl;

    Nodo *indice = new Nodo();

    indice = first_data;

    if(first_data != NULL){
        while(indice != NULL){
            cout << " " << indice->dato;
            indice = indice->next;
        }
        cout << endl << endl;

    }else{
        cout << "Lista " << nombre_lista << " sin variables. " << endl << endl;   
    }
}