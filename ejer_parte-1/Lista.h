#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;

/* Crear la estructura de nodos para la lista */
typedef struct _Nodo {
    int dato;
    struct _Nodo *next;
} Nodo;

class Lista {

    private:
        Nodo *first_data = NULL;
        Nodo *last_data = NULL;

    public:
        Lista();

        void add_nodo_desordenado();

        void add_nodo_ordenado_positivo(Lista *nodos_base);

        void add_nodo_ordenado_negativo(Lista *nodos_base);

        void mostrar_lista(string nombre_lista);

};
#endif